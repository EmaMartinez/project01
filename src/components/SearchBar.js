import React from 'react';

class SearchBar extends React.Component {
    state = {
        term: '',
    };

    onInputChange = (event) => {
        this.setState({term: event.target.value});
    };

    onFormSubmit = (event) => {
        event.preventDefault();
        

        this.props.onFormSubmit(this.state.term);

    };


    render () {
        return (
            <div className="ui search">
                <form onSubmit={this.onFormSubmit} className="ui form">
                    <div className="ui icon input">
                        <input 
                            type="text" 
                            value={this.state.term}
                            onChange={this.onInputChange}
                            placeholder="Search..." />
                        <i class="search icon" />
                    </div>
                </form>
            </div>
        );
    }
}

export default SearchBar;