import React from 'react';

const Form = () => {
    return (
        <div>
            <form class="ui form">
                <div class="field">
                    <label>Title</label>
                    <input type="text" name="first-name" placeholder="Title" />
                </div>
                <div class="field">
                    <label>Description</label>
                    <input type="text" name="description" placeholder="Description" />
                </div>
                <div class="field">
                    <label>Author</label>
                    <input type="text" name="author" placeholder="Author" />
                </div>
                <div class="field">
                    <label>Creation date</label>
                    <input type="date" name="date" placeholder="Date DD/MM/YYYY" />
                </div>
                <div class="field">
                    <label>Text</label>
                    <textarea></textarea>
                </div>
                <div class="inline field">
                    <div class="ui checkbox">
                        <input type="checkbox" name="published" />
                        <label>Published</label>
                    </div>
                </div>
                <button class="ui button" type="submit">Submit</button>
            </form>
        </div>
    )
}

export default Form;